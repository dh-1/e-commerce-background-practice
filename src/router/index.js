import Vue from 'vue'
import VueRouter from 'vue-router'
import denglu from "@/views/denglu"
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: denglu
  }
]

const router = new VueRouter({
  routes
})

export default router
